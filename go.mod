module dtm_test

go 1.16

require (
	github.com/dtm-labs/dtm-examples v0.0.0-20220406014226-1df24e79096c
	github.com/dtm-labs/dtmcli v1.13.0
	github.com/go-resty/resty/v2 v2.7.0
)
