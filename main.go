package main

import (
	"github.com/dtm-labs/dtm-examples/dtmutil"
	"github.com/dtm-labs/dtmcli"
	"github.com/dtm-labs/dtmcli/logger"
	"github.com/go-resty/resty/v2"
)

var bookAPI = "http://localhost:4000"
var accountAPI = "http://localhost:2000"

type BorrowBook struct {
	BookId string `json:"book_id"`
	UserId string `json:"user_id"`
}

func main() {
	borrowBook := &BorrowBook{
		BookId: "625550fa9a4ea0bb451a1f66",
		UserId: "62590afadaba372e9838b9d2",
	}
	gid := dtmcli.MustGenGid(dtmutil.DefaultHTTPServer)
	err := dtmcli.TccGlobalTransaction(dtmutil.DefaultHTTPServer, gid, func(tcc *dtmcli.Tcc) (*resty.Response, error) {
		resp, err := tcc.CallBranch(borrowBook, accountAPI+"/dtm/borrow-book-tcc-try",
			accountAPI+"/dtm/borrow-book-tcc-confirm", accountAPI+"/dtm/borrow-book-tcc-cancel")
		if err != nil {
			return resp, err
		}
		return tcc.CallBranch(borrowBook, bookAPI+"/dtm/borrow-book-tcc-try", bookAPI+"/dtm/borrow-book-tcc-confirm", bookAPI+"/dtm/borrow-book-tcc-cancel")
	})
	logger.FatalIfError(err)
	logger.Infof("Success")
}
